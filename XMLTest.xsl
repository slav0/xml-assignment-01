<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fn="http://www.w3.org/2005/xpath-functions" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:array="http://www.w3.org/2005/xpath-functions/array" xmlns:map="http://www.w3.org/2005/xpath-functions/map" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:err="http://www.w3.org/2005/xqt-errors" exclude-result-prefixes="array fn map math xhtml xs err" version="3.0">
	<xsl:output method="html" version="5.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/" name="xsl:initial-template">
	<!-- Nice work here, Slavo.  Your XSL properly formats and outputs the xml data.
Good work
10/10
-->
	<html>
		<head>
			<title>Telephone Bill</title>
			<h2>Phone number: <xsl:value-of select="telephoneBill/customer/@phoneNumber"/></h2>
			<h2>Name: <xsl:value-of select="telephoneBill/customer/name"/></h2>
			<h2>Address: <xsl:value-of select="telephoneBill/customer/address"/></h2>
			<h2>City: <xsl:value-of select="telephoneBill/customer/city"/></h2>
			<h2>Province: <xsl:value-of select="telephoneBill/customer/province"/></h2>
		</head>
		<body>
		<table border="1" cellpadding="2" cellspacing="2">
			<tbody>
				<tr><th>Call</th></tr>
				<tr>
					<th>Number:</th>
					<th>Date:</th>
					<th>Duration (in mins):</th>
					<th>Charge ($):</th>
				</tr>
				<xsl:for-each select="telephoneBill/call">
				<tr>
					<td><xsl:value-of select="@number"/></td>
					<td><xsl:value-of select="@date"/></td>
					<td><xsl:value-of select="@durationInMinutes"/></td>
					<td><xsl:value-of select="@charge"/></td>
				</tr>
				</xsl:for-each>
			</tbody>
		</table>
		</body>
	</html>
	</xsl:template>
</xsl:stylesheet>
